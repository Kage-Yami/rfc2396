# RFC2396 - Rust <!-- omit in toc -->

- [Overview](#overview)
  - [Versioning](#versioning)
  - [Repository information](#repository-information)
- [Usage](#usage)
  - [Limitations](#limitations)

## Overview

A Rust library for validating strings as RFC2396-compliant URIs

[![docs.io documentation](https://img.shields.io/badge/docs.io-Documentation-orange?style=for-the-badge)](https://docs.rs/rfc2396)
[![crates.io version](https://img.shields.io/crates/v/rfc2396?style=for-the-badge)](https://docs.rs/rfc2396)
[![crates.io downloads](https://img.shields.io/crates/d/rfc2396?style=for-the-badge)](https://crates.io/crates/rfc2396)
[![Gitlab pipeline status](https://img.shields.io/gitlab/pipeline/Kage-Yami/rfc2396-rust/main?style=for-the-badge)](https://gitlab.com/Kage-Yami/rfc2396-rust/pipelines/main/latest)
[![Gitlab code coverage](https://img.shields.io/gitlab/coverage/Kage-Yami/rfc2396-rust/main?style=for-the-badge)](https://gitlab.com/Kage-Yami/rfc2396-rust)
[![Lines of code](https://img.shields.io/tokei/lines/gitlab/Kage-Yami/rfc2396-rust?style=for-the-badge)](https://gitlab.com/Kage-Yami/rfc2396-rust)
[![Dependents](https://img.shields.io/librariesio/dependent-repos/cargo/rfc2396?style=for-the-badge)](https://libraries.io/cargo/rfc2396)
[![License](https://img.shields.io/crates/l/rfc2396?style=for-the-badge)](https://gitlab.com/Kage-Yami/rfc2396-rust/-/blob/main/LICENSE)

### Versioning

This project follows [Semantic Versioning principals](https://semver.org/) starting with `1.0.0`.

### Repository information

This repository is located on [GitLab.com](https://gitlab.com/Kage-Yami/rfc2396-rust).

## Usage

To use this crate, simply call one of the `validate*` functions, depending on your use case and desired return type:

- `validate(S) -> bool`
- `validate_nom(S) -> nom::IResult<&str, &str>`
- `validate_opt(S) -> Option<&str>`
- `validate_res(S) -> anyhow::Result<&str>`

Where `S` is some type that implements `Into<&str>`; for example, `&str` itself!

All functions except the `_nom` variant will additionally ensure that the entire input is consumed and matches exactly what was parsed internally. If you require that this check is not done (e.g. as part of a larger parsing exercise), then use the `_nom` variant.

### Limitations

This crate does not provide a data structure with which components of a URI are represented by.
