use anyhow::Context;

test_cases! {
    component::path_segments: {
        case_001: "!'S=P8wv@btuC4-Ln6:%95nN+6!",
        case_002: "!j%84Ni$5NxJ)oluOE/I+wxc.C&",
        case_003: "&K,W3R~CRvq=9e)c-eYaY7;F,",
        case_004: "&Nw:w&WKWBy1j66JfG~5RH2tX",
        case_005: "'-7.tier,9NkUS19ZSGNrufEJ",
        case_006: "'92LNb@GwZ!/S_Mp&QK=(e0Xm",
        case_007: "(Cm-lN@Un$.1k)4xlN/RNXK_Q",
        case_008: "(ODM5/~f-@LeMcSsrynwKeo:V",
        case_009: "(_%4DIfmy4_TUE%13=5cgs$)Wg@7W",
        case_010: ")1h-A*VA')XZ_XoXhEIY@21ZE",
        case_011: "*Av&:&-%5FgU:6%DC)HnP%2DCb+OlM7",
        case_012: "*N)uzUCXK5RXn,ldtWpJgI;Vp",
        case_013: "+N~Q*g@X)WxNm;W8Ms_x0:XqR",
        case_014: ",9aDWI$+hQ)xj;uv)QfQ.&1@K",
        case_015: ",R&~&s_SiNJo,g1HnCW3DGAKf",
        case_016: "-5njnk7W0X(!eh0(a1cAdS!j8",
        case_017: ".Iq8uOYwX5Mc0%1Ei6NPL+qUfh%17",
        case_018: ".Yz-T6;AdRn!C:',ZFw=_8.qL",
        case_019: ".os..Pb%77yuuwc8$xFCE&_oC6y",
        case_020: "/6ylbKZqMTx4CFlr32a_G45se",
        case_021: "/nJxv+X4PkKUi=t~,pwcdlpj.",
        case_022: "1ReH,.PauGf.$gkouA.8yx*_x",
        case_023: "1h~nWtrw8eJafh-!7cUBciwq4",
        case_024: "3i(%CAxsO@98;zS,A%B3OGQk-KuVU",
        case_025: "54SAS2f*D.w$lcNSh_x=$/i=-",
        case_026: "5_H3uuXuZazc.W~Y:OYavdksc",
        case_027: "6ld$wDaD%1E*fPkZ~j@PnKV10N/",
        case_028: "7CM.o=6l)+u,uzj~s&I1$nCY0",
        case_029: "8LcgvlX@:No)4hS!!%A7~kEZuyO",
        case_030: "8WJR6@!!bBL!Y0H.ZX4gyLh=2",
        case_031: "8bj/',XeN=H.a(JkbRD*~n=,T",
        case_032: "@D/nVa@J1*p@sQoe)myFxx8,n",
        case_033: "@e1aMcwjK9Q6tV$Th~E&*ZH9D",
        case_034: "Ar5HPx05KSf_@.R;g'2h_$gTQ",
        case_035: "BXSxXiFs$GffapKf&U-'@$5A4",
        case_036: "BcbqB8qLDpH63Ue6eNpJIBzg5",
        case_037: "CfFky!'Kyr-nbs&AW@Cr$-Y(~",
        case_038: "D*M*6';39YI:C)+9Sk!,Nxt5:",
        case_039: "DjqOWf8'q/9eU/RMD4HfbW-W&",
        case_040: "E!1&o1&RJ0W:CODFo:uS8:Xal",
        case_041: "EIdxCd%C5WvZO(FCU2.i$z~T)1C",
        case_042: "EQ'/2Dqnfn)y9fOQ&DMrkwJM&",
        case_043: "EeQ%BFhLK&.qe07qTQY)U:!Euc+",
        case_044: "F+IG4nbiMl=ub8*jKc_atU5/I",
        case_045: "G)4dHX7Nz7XB0~TWy:4b078UF",
        case_046: "G~w-Gz(fqO77.~@RCA=jbAd4~",
        case_047: "H6l@fgTXZ.FdldFM%13tw(*:l+V",
        case_048: "Is5;WtWnvCM3Qeog.H'1l6)U/",
        case_049: "JawW2:h626:093yAvoJO1HLVJ",
        case_050: "K$X1;WWo9HQ;XR@os03(;JPSD",
        case_051: "K5(G)A&VNqq-:7(DvKr_9K5~X",
        case_052: "KJn'5j@_0Zr5D@W=Y;W58Lhwb",
        case_053: "L6t0nnF5qT-%D7$a'MQLnrA=r:*",
        case_054: "M5148s,xw:YPFZa,d2s%94j3czz",
        case_055: "N;xAE;QYalI=gN-ZSv*-'.$Qc",
        case_056: "Nq8OU%6DTtE8WHhQ65VDtFe1n9_",
        case_057: "OyO.PRy(-m(K//58Wcz*Y;br:",
        case_058: "P~h-H5RVH%236PC=RDeQcY3Cl;G",
        case_059: "Q&8lt=PfW-ytY~MS=J&_I.'qR",
        case_060: "T$Y7(xP=z&7cC.k43ocwR=-_,",
        case_061: "T@HZsj,xw0dzm!Hdo$lS2SrP@",
        case_062: "TMno7IP~RzMBg'ARAWeX8CbVG",
        case_063: "VjJrGs7JBNixbF-&zRd'JsucP",
        case_064: "X(2ep!%53Lst$kSQj+46hGIi2*H",
        case_065: "XLzE,@XF0hrhl0iz61Tg6%57F0F",
        case_066: "XV=)Qxa&Oi9dqN+lK'pf;:Tk-",
        case_067: "X~&1ABpJ3PhrDmzaNg+WOs8pa",
        case_068: "Zg3dUw&G~FJcOpRglV+R/onLc",
        case_069: "_!Fn%FAPE1gw(Sj*.9$Wbs1/c,a",
        case_070: "_M6q=Vuq@Dy=!Lhq3zo*E6TiD",
        case_071: "_pZWv*ld-GhnxE/a3=0+:/L5,",
        case_072: "bg$&J5q*T=sX(p$//JahTaEUk",
        case_073: "dJqzcnu9N90TvM.L$gDxdX9qs",
        case_074: "fBA=O7q1CCqh@RxBL%88Z:Y'$Mp",
        case_075: "fW1jPqatJlH@5'bG@5WWS1Z9b",
        case_076: "gKkKWuakKi-jsSH'dT'U:*16n",
        case_077: "hPycS5y=~g:wkRmW;02a@3M4S",
        case_078: "j9_-@VObobGFd%2C&S.eK)%A5ETV@",
        case_079: "k8I@xJLk(eFY:E~Cho-7zDy:D",
        case_080: "ll.O4$(i%2FxdpAU05l6eeacZq0",
        case_081: "oG@+z5qljM7o'nGQZH1fLrYIQ",
        case_082: "pqt:c,H$hpZ+$tNdvSvgI(Y+q",
        case_083: "qti5vnYZ(d&ze_JAL-CEN%1Cu$f",
        case_084: "s=JNNG.*!P:bPV9+eNvxd%33P;6",
        case_085: "sWgebBHgSTy(zuaRvXKub5Wwn",
        case_086: "t*xu3b5:XfD!ebG3B;bW07kFg",
        case_087: "t.OIofIwS+T%66U%093GWHq/+8q@o",
        case_088: "t4Tu-E@H119@*9o:pqlUlIL'Q",
        case_089: "u/FB/p8Qu0B9a1EH;JGny~siK",
        case_090: "udm)VHh+EoHGm~Jzn=8eRh(h$",
        case_091: "uwO+b~7FVa7;optup3byNvp~s",
        case_092: "v@r,lAOxcPl8ZTEZ%B8djm(&hWc",
        case_093: "vd*:r/O=,86dX-4E1R2&y$w(,",
        case_094: "vhP$,_/-&@$$LRj4awL36f@b;",
        case_095: "x3mVsiz_Sp+PICJp@4g9)TjaG",
        case_096: "xijIZ93L=:f7km_~4c,Zq8BM5",
        case_097: "zCqIx*h=eNLyn;aK45'OK9ICE",
        case_098: "zcjynb-qnHpjw_(uPOcO!:nuF",
        case_099: "zz-*c+w0!Rzhoj1ekSZuJckyN",
        case_100: "~&5ZK*vJD!I(c(eR&8RS:mxIS"
    }
}
