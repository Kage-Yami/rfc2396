use anyhow::Context;

test_cases! {
    char::reserved: {
        case_ampersand: "&",
        case_at: "@",
        case_colon: ":",
        case_comma: ",",
        case_dollar: "$",
        case_equals: "=",
        case_plus: "+",
        case_question: "?",
        case_semicolon: ";",
        case_forward_slash: "/"
    }
}
