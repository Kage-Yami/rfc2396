macro_rules! test_cases {
    (
        $( $case:ident: $text:literal ),+
    ) => {$(
        #[test]
        fn $case() {
            assert_eq!(rfc2396::validate_opt($text), Some($text));
        }
    )+};
}

include!("test_cases/mod.rs");
